﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdevMat.Models
{
    public class Cube : Movable
    {
        public float posX, posY;
        public Color color = new Color();
        public Cube(float x = 0, float y = 0)
        {
            this.posX = x;
            this.posY = y;

        }
        //public void Render(OpenGL gl)
        //{
        //    gl.Begin(OpenGL.GL_QUADS);
        //    //gl.Color(color.red, color.green, color.blue);
        //    //Front Face
        //    gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
        //    gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
        //    gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
        //    gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

        //    gl.End();
        //}

        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_QUADS);
            //gl.Color(color.red, color.green, color.blue);
            //Front Face
            gl.Vertex(this.posX - 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY + 0.5f);
            gl.Vertex(this.posX + 0.5f, this.posY - 0.5f);
            gl.Vertex(this.posX - 0.5f, this.posY - 0.5f);

            gl.End();
        }
    }
    
}
