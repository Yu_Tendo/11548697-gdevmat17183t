﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gdevmat_17183t.Models
{
    public class Liquid
    {
        public float x, y;
        public float width, depth;
        public float drag;

        public Liquid(float x, float y, float width , float depth, float drag)
        {
            this.x = x;
            this.y = y;
            this.width = depth;
            this.depth = depth;
            this.drag = drag;
        }
        public void Render(OpenGL gl, byte r = 28, byte g = 120, byte b = 186)
        {
            gl.Color(r, g, b);
            gl.Begin(OpenGL.GL_POLYGON);
            gl.Vertex(x - width, y, 0);
            gl.Vertex(x + width, y, 0);
            gl.Vertex(x + width, y - depth, 0);
            gl.Vertex(x - width, y - depth, 0);
            gl.End();

        }

        /**
        
         * checks if the position of a movable 
         * is inside the actual <code><Liquid</code>
     
         **/
        public bool Contains(Movable movable)
        {
            var p = movable.Position;
            return p.x > this.x - this.width &&
                p.x < this.x + this.width &&
                p.y < this.y;
        }
        
        public Vector3 CalculateDragForce(Movable movable)
        {
            //Magnitude is coeffcient * speed squared;
            var speed = movable.Velocity.GetMagnitude();
            var dragMagnitude = this.drag * speed * speed;

            //Direction is inverse of velocity
            var dragForce = movable.Velocity;
            dragForce *= -1;

            //Scale accprdomg tp ,agmotide
            dragForce.Normalize();
            dragForce *= dragMagnitude;

            return dragForce;

        }
    }
}
