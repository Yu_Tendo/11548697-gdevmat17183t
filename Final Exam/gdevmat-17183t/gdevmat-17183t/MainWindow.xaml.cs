﻿using gdevmat_17183t.Models;
using gdevmat_17183t.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;


namespace gdevmat_17183t
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        int b = 6;
        private Vector3 mousePosition = new Vector3();

        private List<Cube> cubes = new List<Cube>();
        private List<Circle> circles = new List<Circle>();
        private List<Cube> block = new List<Cube>();


        private Vector3 wind = new Vector3(0.05f, 0, 0);
        private Vector3 gravity = new Vector3(0, -0.5f, 0);

        
        public Cube blocks = new Cube()
        {

            Position = new Vector3(65.0f, 35.0f, 0),
            Scale = new Vector3(10 / 1.5f, 4 / 1.5f, 0 / 1.5f)
        };
        public Cube bouncer = new Cube()
        {
            Position = new Vector3(0, -35.0f, 0),
            Scale = new Vector3(16 / 1.5f, 2 / 1.5f, 0 / 1.5f)
        };
        private Cube ball = new Cube()
        {
            Position = new Vector3(0, 0, 0),
            Scale = new Vector3(2 / 1.5f, 2 / 1.5f, 2 / 1.5f),
            color = new Models.Color((float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 100)),
            Velocity = new Vector3(),
            Acceleration = new Vector3(0.0f, 0.10f, 0.0f),

        };
        //private Cube myCube = new Cube()
        //{

        //    Position = new Vector3(0, 30, 0),
        //    Mass = 5,
        //    color = new Models.Color(RandomNumberGenerator.GenerateInt(0, 1), RandomNumberGenerator.GenerateInt(0, 1), RandomNumberGenerator.GenerateInt(0, 1), RandomNumberGenerator.GenerateInt(0, 1)),
        //};


        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT Final";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -100.0f);


            bouncer.Render(gl);
            if (Keyboard.IsKeyDown(Key.D)) bouncer.Position.x += 2;
            if (Keyboard.IsKeyDown(Key.A)) bouncer.Position.x -= 2;
            if (bouncer.Position.x >= 65) bouncer.Position.x -= 2;
            if (bouncer.Position.x <= -65) bouncer.Position.x += 2;
            ball.Render(gl);
            ball.ApplyForce(wind);
            ball.ApplyForce(gravity * 0.10f);
            foreach (var c in block)
            {

                foreach (var cc in block)
                {

                    if (c != cc)
                    {

                        c.Render(gl);
                        
                    }
                }
            }

            if (ball.Position.y >= 40) {
                ball.Velocity.y *= -1.0f;
                ball.Position.y = 40;
                ball.ApplyGravity(-0.5f);

            }

            if (ball.Position.x >= 65) { 
                ball.Velocity.x *= -1.0f;
                ball.Position.x = 65;
            }
            if (ball.Position.x <= -65) {
                ball.Velocity.x *= -1.0f;
                ball.Position.x = 65;
            }

            if (ball.HasCollidedWith(bouncer) == false)
            {
                ball.Velocity.y *= -2.0f;

            }
           //if (ball.HasCollidedWith(block[i]) == false)
           // {
           //         ball.Velocity.y *= 1.0f;

           // }
        








            gl.DrawText(0, 0, 1, 1, 1, "Arial", 25, " " + counter + "\n");
        }
        int counter = 0;


        #region INITIALIZATION
        public MainWindow()
        {

            InitializeComponent();

            for (int a = 1; a <= 6; a++)

            {

                block.Add(new Cube()
                {
                    Position = new Vector3(65.0f - (a * 20.0f), 35.0f, 0),

                    Scale = new Vector3(10 / 1.5f, 4 / 1.5f, 0 / 1.5f),
                    color = new Models.Color((float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 1), (float)RandomNumberGenerator.GenerateInt(0, 100)),

                });


            }


        }
        private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            OpenGL gl = args.OpenGL;

            gl.Enable(OpenGL.GL_DEPTH_TEST);

            float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
            float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
            float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
            float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

            float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

            gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
            gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
            gl.Disable(OpenGL.GL_LIGHTING);
            gl.Disable(OpenGL.GL_LIGHT0);

            gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
            gl.Enable(OpenGL.GL_BLEND);
            gl.ClearColor(0, 0, 0, 0);


            gl.ShadeModel(OpenGL.GL_SMOOTH);


        }

        #endregion

        private void OpenGLControl_MouseMove(object sender, MouseEventArgs e)
        {
            var position = e.GetPosition(this);
            mousePosition.x = (float)position.X - (float)Width / 2.0f;
            mousePosition.y = -((float)position.Y - (float)Height / 2.0f);
        }
    }
}
