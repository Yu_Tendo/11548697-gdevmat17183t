﻿using GdevMat.Models;
using GdevMat.Utilities;
using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace GdevMat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Cube myCube = new Cube(new Vector3(-5, 3, 0));
        public Vector3 direction = new Vector3(5, 5, 0);

        private List<Circle> circles = new List<Circle>();
        private List<Cube> cubes = new List<Cube>();
        private void OpenGLControl_OpenGLDraw(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
        {
            this.Title = "GDEVMAT DAY 5";
            OpenGL gl = args.OpenGL;

            // Clear the screen and the depth buffer

            // Clear previous frame and display new one
            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.Translate(0.0f, 0.0f, -20.0f);
            
            

            gl.LineWidth(5);
            gl.Color(1.0f, 0.0f, 0.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 0);
            gl.Vertex(direction.x, direction.y);
            gl.End();

            gl.LineWidth(3);
            gl.Color(1.0f, 1.0f, 1.0f);
            gl.Begin(OpenGL.GL_LINE_STRIP);
            gl.Vertex(0, 0);
            gl.Vertex(direction.x, direction.y);
            gl.End();
            direction.Normalize();
            direction *= 2;
            gl.DrawText( 0, 0, 1, 1, 1, "Arial", 25, direction.GetMagnitude().ToString());
        }

      
            #region  INITIALIZATION

            public MainWindow()
            {
                direction *= 2;
                InitializeComponent();
            }

            private void OpenGLControl_OpenGLInitialized(object sender, SharpGL.SceneGraph.OpenGLEventArgs args)
            {
                OpenGL gl = args.OpenGL;

                gl.Enable(OpenGL.GL_DEPTH_TEST);

                float[] global_ambient = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
                float[] light0pos = new float[] { 0.0f, 5.0f, 10.0f, 1.0f };
                float[] light0ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
                float[] light0diffuse = new float[] { 0.3f, 0.3f, 0.3f, 1.0f };
                float[] light0specular = new float[] { 0.8f, 0.8f, 0.8f, 1.0f };

                float[] lmodel_ambient = new float[] { 0.2f, 0.2f, 0.2f, 1.0f };
                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

                gl.LightModel(OpenGL.GL_LIGHT_MODEL_AMBIENT, global_ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_POSITION, light0pos);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_AMBIENT, light0ambient);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_DIFFUSE, light0diffuse);
                gl.Light(OpenGL.GL_LIGHT0, OpenGL.GL_SPECULAR, light0specular);
                gl.Disable(OpenGL.GL_LIGHTING);
                gl.Disable(OpenGL.GL_LIGHT0);

                gl.BlendFunc(OpenGL.GL_SRC_ALPHA, OpenGL.GL_ONE_MINUS_CONSTANT_ALPHA_EXT);
                gl.Enable(OpenGL.GL_BLEND);
                gl.ClearColor(0, 0, 0, 0);

                gl.ShadeModel(OpenGL.GL_SMOOTH);
            }
            #endregion
        }
    }


//(-19,0,10,20,30) = 10     - (-10-10)^2 + (0-10)^2 + (10-10)^2 + (20-10)^2 + (30-10)^2 
//  subtract                     _________________________________________________________
//     mean                                          5

    
    
    //myCube.Position += direction;
            //if (myCube.Position.x >= 30)
            //{
            //    direction.x = 1;
            //}
            //else if (myCube.Position.x <= -30)
            //{
            //    direction.x = -1;
            //}
            //if (myCube.Position.y >= 15)
            //{
            //    direction.y = -1;
            //}
            //if (myCube.Position.y >= -15)
            //{
            //    direction.y = 1;
            //}
            //myCube.Render(gl);