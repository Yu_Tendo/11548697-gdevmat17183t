﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdevMat.Models
{
    public class Color
    {
        public double red;
        public double green;
        public double blue;
        public double alpha = 1;

        public Color(double r = 1, double g = 1, double b = 1)
        {
            this.red = r;
            this.green = g;
            this.blue = b;
        }
    }
}
