﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GdevMat.Models
{
    public class Cube : Movable
    {
        //public float Position.x.X, Position.y;
        //public Color color = new Color();
        public Cube(float x = 0, float y = 0, float z = 0)
        {
            this.Position.x = x;
            this.Position.x = y;
            this.Position.x = z;

        }
       public Cube(Vector3 initPos)
        {
            this.Position = initPos;
        }
        public override void Render(OpenGL gl)
        {
            gl.Color(red, green, blue, alpha);
            gl.Begin(OpenGL.GL_QUADS);
            //gl.Color(color.red, color.green, color.blue);
            //Front Face
            gl.Vertex(this.Position.x - 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y + 0.5f);
            gl.Vertex(this.Position.x + 0.5f, this.Position.y - 0.5f);
            gl.Vertex(this.Position.x - 0.5f, this.Position.y - 0.5f);

            UpdateMotion();
            gl.End();
        }

        private void UpdateMotion()
        {
            this.Velocity += this.Acceleration;
            this.Position += this.Velocity;
            //this.Acceleration *= 0;
            
        }
    }
    
}
